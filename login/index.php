<?
  session_start();
  require('../pengaturan/helper.php');
  cekIzinAksesHalaman(null, $alamat_web, true);
  $judul_halaman = "Login Pengguna";
?>
<?php if(isset($_GET['status'])): ?>
  <?php if($_GET['status'] == 'gagal'): ?>
  <div>
    <p>Username atau password salah!</p> 
  </div>
  <?php endif; ?>
<?php endif; ?>
<!doctype html>
<html lang="en" dir="ltr">
  <head>
  <?php
    include("../template/head.php");
  ?>
</head>
<body>
  <div class="page">
      <div class="page-single">
        <div class="container">
          <div class="row">
            <div class="col col-login mx-auto">
              <div class="text-center mb-6">
                <img src="./assets/brand/tabler.svg" class="h-6" alt="">
              </div>
                <form class="card" action="<?=$alamat_web?>/login/proses-login.php" method="POST">
                  <div class="card-body p-6">
                    <div class="card-title">Login</div>
                    <div class="form-group">
                      <label class="form-label">Username</label>
                      <input class="form-control" type="text" name="username" />
                    </div>
                    <div class="form-group">
                      <label class="form-label">Password</label>
                      <input class="form-control" type="password" name="password" />
                    </div>
                    <div class="form-group">
                      <button class="btn btn-primary"   type="submit" class="btn btn-primary btn-block">Login</button>
                      <button class="btn btn-primary"   type="reset" class="btn btn-danger btn-block">Reset</button>
                    </div>
                  </div>
                </form>
                <div class="text-center text-muted">
                  Anda harus login sebelum menggunakan aplikasi
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</body>
</html>
