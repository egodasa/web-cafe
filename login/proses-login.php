<?
  session_start();
  require('../pengaturan/helper.php');
  if($_SERVER['REQUEST_METHOD'] == 'POST'){
    require_once('../pengaturan/database.php');
    $query = $db->prepare("SELECT username, level FROM tbl_user WHERE username = :username AND password = md5(:password) LIMIT 1");
    $query->bindParam('username', $_POST['username']); 
    $query->bindParam('password', $_POST['password']); 
    $query->execute();
    $data = $query->fetch();
    // Cek apakah username betul atau tidak
    if($data){
      $_SESSION['username'] = $data['username'];
      $_SESSION['level'] = $data['level'];
      // Cek level agar halaman di redirect sesuai aktor
      if($_SESSION['level'] == "Kasir"){
        header("Location: $alamat_web/pembayaran");
      }else if($_SESSION['level'] == "Dapur"){
        header("Location: $alamat_web/dapur");
      }
    }else{
      header("Location: $alamat_web/login?status=gagal");
    }
  }else{
    header("Location: $alamat_web/login");
  }
?>
