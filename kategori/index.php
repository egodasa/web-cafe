<?php
  session_start();
  require("../pengaturan/helper.php");
  cekIzinAksesHalaman(array('Kasir'), $alamat_web);
  $judul_halaman = "Daftar Kategori";
  require("../pengaturan/database.php");
  $query = $db->prepare("SELECT * FROM tbl_kategori"); 
  $query->execute();
  $data = $query->fetchAll();
?>
<html>
<head>
  <?php
    include("../template/head.php");
  ?>
</head>
<body>
<a href="<?=$alamat_web?>/kategori/tambah.php">Tambah</a>
<table class="table card-table table-vcenter text-nowrap" >
  <thead>
    <tr>
      <th>No</th>
      <th>Nama Kategori</th>
      <th>Aksi</th>
    </tr>
  </thead>
  <tbody>
<?php
$no = 1;
if(count($data) > 0){
  foreach($data as $d){
?>
    <tr>
      <td><?=$no?></td>
      <td><?=$d['nm_kategori']?></td>
      <td>
        <a href="<?=$alamat_web?>/kategori/proses_hapus.php?id_kategori=<?=$d[id_kategori]?>">Hapus</a> 
        | <a href="<?=$alamat_web?>/kategori/edit.php?id_kategori=<?=$d[id_kategori]?>">Edit</a></td>
    </tr>
<?php 
  $no++;
  }
}else{
?>
    <tr>
      <td colspan=3>Tidak ada data yang ditampilkan!</td>
    </tr>
<?php
}
?>
  </tbody>
</table>

  <?php include("../template/script.php"); ?>
</body>
</html>
