<?php
  session_start();
  require("../pengaturan/helper.php");
  cekIzinAksesHalaman(array('Kasir'), $alamat_web);
  $judul_halaman = "Tambah Kategori";
?>
<html>
<head>
  <?php
    include("../template/head.php");
  ?>
</head>
<body>
<form class="card"  method="POST" action="<?=$alamat_web?>/kategori/proses_tambah.php">
  <div>
    <label class="form-label" >Nama Kategori</label>
    <input class="form-control"  type="text" name="nm_kategori" required />
  </div>
  <div>
    <button class="btn btn-primary btn-block"   type="submit">Simpan</button>
    <button class="btn btn-primary btn-block"   type="reset">Reset</button>
  </div>
</form>

  <?php include("../template/script.php"); ?>
</body>
</html>
