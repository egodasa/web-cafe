<?php
  session_start();
  cekIzinAksesHalaman(array('Kasir'), $alamat_web);
  $judul_halaman = "Edit Kategori";
  if(isset($_GET['id_kategori'])){
    require_once("../pengaturan/helper.php");
    require_once("../pengaturan/database.php");
    $query = $db->prepare("SELECT * FROM tbl_kategori WHERE id_kategori = ? LIMIT 1"); 
    $query->bindParam(1, $_GET['id_kategori']);
    $query->execute();
    $detail = $query->fetch();
    
    // cek dulu, datanya ketemu atau tidak. Kalau gk ketemu, ya redirect ke halaman awal
    if(empty($detail)){
      header('Location: $alamat_web/kategori');
    }
  }else{
    header('Location: $alamat_web/kategori');
  }
?>
<html>
<head>
  <?php
    include("../template/head.php");
  ?>
</head>
<body>
<form class="card"  method="POST" action="<?=$alamat_web?>/kategori/proses_edit.php">
  <input class="form-control"  type="hidden" name="id_kategori" value="<?=$detail['id_kategori']?>" />
  <div>
    <label class="form-label"  class="form-label" >Nama Kategori</label>
    <input class="form-control"  type="text" name="nm_kategori" value="<?=$detail['nm_kategori']?>" required />
  </div>
  <div>
    <button class="btn btn-primary btn-block"   type="submit">Simpan perubahan</button>
    <button class="btn btn-primary btn-block"   type="reset">Reset</button>
  </div>
</form>

  <?php include("../template/script.php"); ?>
</body>
</html>
