<?php
  session_start();
  require("../pengaturan/helper.php");
  cekIzinAksesHalaman(array('Kasir'), $alamat_web);
  $judul_halaman = "Tambah Meja";
  require("../pengaturan/helper.php");
?>
<html>
<head>
  <?php
    include("../template/head.php");
  ?>
</head>
<body>
<form class="card"  method="POST" action="<?=$alamat_web?>/meja/proses_tambah.php">
  <div>
    <label class="form-label" >Nama meja</label>
    <input class="form-control"  type="text" name="nm_meja" required />
  </div>
  <div>
    <button class="btn btn-primary"   type="submit">Simpan</button>
    <button class="btn btn-primary"   type="reset">Reset</button>
  </div>
</form>

  <?php include("../template/script.php"); ?>
</body>
</html>
