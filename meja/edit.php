<?php
  session_start();
  require("../pengaturan/helper.php");
  cekIzinAksesHalaman(array('Kasir'), $alamat_web);
  $judul_halaman = "Edit Meja";
  if(isset($_GET['id_meja'])){
    require_once("../pengaturan/database.php");
    $query = $db->prepare("SELECT * FROM tbl_meja WHERE id_meja = ? LIMIT 1"); 
    $query->bindParam(1, $_GET['id_meja']);
    $query->execute();
    $detail = $query->fetch();
    
    // cek dulu, datanya ketemu atau tidak. Kalau gk ketemu, ya redirect ke halaman awal
    if(empty($detail)){
      header("Location: $alamat_web/meja");
    }
  }else{
    header("Location: $alamat_web/meja");
  }
?>

<html>
<head>
  <?php
    include("../template/head.php");
  ?>
</head>
<body>
<form class="card"  method="POST" action="<?=$alamat_web?>/meja/proses_edit.php">
  <input class="form-control"  type="hidden" name="id_meja" value="<?=$detail['id_meja']?>" />
  <div>
    <label class="form-label" >Nama meja</label>
    <input class="form-control"  type="text" name="nm_meja" value="<?=$detail['nm_meja']?>" required />
  </div>
  <div>
    <button class="btn btn-primary"   type="submit">Simpan perubahan</button>
    <button class="btn btn-primary"   type="reset">Reset</button>
  </div>
</form>

  <?php include("../template/script.php"); ?>
</body>
</html>
