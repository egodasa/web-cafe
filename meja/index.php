<?php
  session_start();
  require("../pengaturan/helper.php");
  cekIzinAksesHalaman(array('Kasir'), $alamat_web);
  $judul_halaman = "Daftar Meja";
  require("../pengaturan/database.php");
  $query = $db->prepare("SELECT * FROM tbl_meja"); 
  $query->execute();
  $data = $query->fetchAll();
?>

<html>
<head>
  <?php
    include("../template/head.php");
  ?>
</head>
<body>
<a href="<?=$alamat_web?>/meja/tambah.php">Tambah</a>
<table class="table card-table table-vcenter text-nowrap" >
  <thead>
    <tr>
      <th>No</th>
      <th>Nama meja</th>
      <th>Aksi</th>
    </tr>
  </thead>
  <tbody>
<?php
$no = 1;
if(count($data) > 0){
  foreach($data as $d){
?>
    <tr>
      <td><?=$no?></td>
      <td><?=$d['nm_meja']?></td>
      <td>
        <a href="<?=$alamat_web?>/meja/proses_hapus.php?id_meja=<?=$d[id_meja]?>">Hapus</a> 
        | <a href="<?=$alamat_web?>/meja/edit.php?id_meja=<?=$d[id_meja]?>">Edit</a></td>
    </tr>
<?php 
  $no++;
  }
}else{
?>
    <tr>
      <td colspan=3>Tidak ada data yang ditampilkan!</td>
    </tr>
<?php
}
?>
  </tbody>
</table>

  <?php include("../template/script.php"); ?>
</body>
</html>
