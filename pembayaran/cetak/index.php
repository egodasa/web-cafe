<?php
  session_start();
  require("../pengaturan/helper.php");
  cekIzinAksesHalaman(array('Kasir'), $alamat_web);
  if(isset($_GET['id_pesan'])){
    require_once("../../pengaturan/database.php");
    // Ambil detail pesan
    $query = $db->prepare("SELECT a.*,b.nm_meja FROM tbl_pesan a JOIN tbl_meja b ON a.id_meja = b.id_meja WHERE a.id_pesan = :id_pesan LIMIT 1");
    $query->bindParam("id_pesan", $_GET['id_pesan']); 
    $query->execute();
    $detail_pesan = $query->fetch();
    
    
    // Ambil daftar pesan
    $query = $db->prepare("SELECT * FROM daftar_pesanan WHERE id_pesan = :id_pesan");
    $query->bindParam("id_pesan", $_GET['id_pesan']); 
    $query->execute();
    $daftar_pesan = $query->fetchAll();
    
  }else{
    header("Location: $alamat_web/pembayaran");
  }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
	
	<title>Struk</title>
	
	<link rel='stylesheet' type='text/css' href='css/style.css' />
	<link rel='stylesheet' type='text/css' href='css/print.css' media="print" />

</head>

<body>

	<div id="page-wrap">

		<textarea id="header">INVOICE</textarea>
		
		<div id="identity">
		
            <textarea id="address">Chris Coyier
123 Appleseed Street
Appleville, WI 53719

Phone: (555) 555-5555</textarea>

            <div id="logo">
              <img id="image" src="images/logo.png" alt="logo" />
            </div>
		
		</div>
		
		<div style="clear:both"></div>
		
		<div id="customer">

            <textarea id="customer-title">Widget Corp.
c/o Steve Widget</textarea>

            <table class="table card-table table-vcenter text-nowrap"  id="meta">
                <tr>
                    <td class="meta-head">No Pesan #</td>
                    <td><textarea><?=$detail_pesan['id_pesan']?></textarea></td>
                </tr>
                <tr>

                    <td class="meta-head">Tanggal Pesan</td>
                    <td><textarea id="date"><?=tanggal_indo_waktu($detail_pesan['tanggal_pesan'])?></textarea></td>
                </tr>
                <tr>
                    <td class="meta-head">Total Bayar</td>
                    <td><div class="due" id="total_bayar">TOTAL BAYAR</div></td>
                </tr>

            </table>
		
		</div>
		
		<table class="table card-table table-vcenter text-nowrap"  id="items">
		
		  <tr>
		      <th>No</th>
		      <th>Nama</th>
		      <th>Harga</th>
		      <th>Jumlah</th>
		      <th>Total Harga</th>
		  </tr>
		  
<?php
$no = 1;
if(count($daftar_pesan) > 0){
  $total_bayar = 0;
  foreach($daftar_pesan as $d){
  $total_bayar += $d['sub_bayar'];
?>
      <tr class="item-row">
        <td class="item-name" style="width:3%;"><div class="delete-wpr"><textarea><?=$no?></textarea></div></td>
        <td class="item-name"><div class="delete-wpr"><textarea><?=$d['nama']?></textarea></div></td>
        <td><textarea class="cost"><?=rupiah($d['harga'])?></textarea></td>
        <td><textarea class="qty"><?=$d['jumlah']?></textarea></td>
        <td><span class="price"><?=rupiah($d['sub_bayar'])?></span></td>
      </tr>
<?php 
  $no++;
  }
}else{
?>
    <tr>
      <td colspan=3>Tidak ada data yang ditampilkan!</td>
    </tr>
<?php
}
?>
		  <tr>
		      <td colspan="2" class="blank"> </td>
		      <td colspan="2" class="total-line">Total</td>
		      <td class="total-value"><div id="subtotal"><?=rupiah($total_bayar)?></div></td>
		  </tr>
		  <tr>

		      <td colspan="2" class="blank"> </td>
		      <td colspan="2" class="total-line">Dibayar</td>
		      <td class="total-value"><div id="total"><?=rupiah($detail_pesan['dibayar'])?></div></td>
		  </tr>
		  <tr>
		      <td colspan="2" class="blank"> </td>
		      <td colspan="2" class="total-line">Kembalian</td>

		      <td class="total-value"><textarea id="paid"><?=rupiah($detail_pesan['kembalian'])?></textarea></td>
		  </tr>
		
		</table>
	
	</div>
<script>
document.getElementById("total_bayar").innerHTML = document.getElementById("subtotal").innerHTML;
</script>
	
</body>

</html>
