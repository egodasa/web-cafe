<?php
  session_start();
  require("../pengaturan/helper.php");
  cekIzinAksesHalaman(array('Kasir'), $alamat_web);
  $judul_halaman = "Detail Pesanan";
  if(isset($_GET['id_pesan'])){
    require_once("../pengaturan/database.php");
    // Ambil detail pesan
    $query = $db->prepare("SELECT a.*,b.nm_meja FROM tbl_pesan a JOIN tbl_meja b ON a.id_meja = b.id_meja WHERE id_pesan = :id_pesan LIMIT 1");
    $query->bindParam("id_pesan", $_GET['id_pesan']); 
    $query->execute();
    $detail_pesan = $query->fetch();
    
    
    // Ambil daftar pesan
    $query = $db->prepare("SELECT * FROM daftar_pesanan WHERE id_pesan = :id_pesan");
    $query->bindParam("id_pesan", $_GET['id_pesan']); 
    $query->execute();
    $daftar_pesan = $query->fetchAll();
    
  }else{
    header("Location: $alamat_web/pembayaran");
  }

?>
<html>
<head>
  <?php
    include("../template/head.php");
  ?>
</head>
<body>
<h2>Daftar Pesanan</h2>
<table class="table card-table table-vcenter text-nowrap" >
  <thead>
    <tr>
      <th>No</th>
      <th>Nama</th>
      <th>Harga</th>
      <th>Sub Bayar</th>
    </tr>
  </thead>
  <tbody>
<?php
$no = 1;
if(count($daftar_pesan) > 0){
  $total_bayar = 0;
  foreach($daftar_pesan as $d){
  $total_bayar += $d['sub_bayar'];
?>
    <tr>
      <td><?=$no?></td>
      <td><?=$d['nama']?></td>
      <td><?=rupiah($d['harga'])?></td>
      <td><?=rupiah($d['sub_bayar'])?></td>
    </tr>
<?php 
  $no++;
  }
}else{
?>
    <tr>
      <td colspan=3>Tidak ada data yang ditampilkan!</td>
    </tr>
<?php
}
?>
  </tbody>
  <tfoot>
    <tr>
      <td colspan=4>Total Bayar</td>
      <td><?=rupiah($total_bayar)?></td>
    </tr>
    </tr>
  </tfoot>
</table>
<h2>Detail Pesanan</h2>
<form class="card"  action="<?=$alamat_web?>/pembayaran/proses-pembayaran.php" method="POST">
  <div>
    <label class="form-label" >No Pesan</label>
    <input class="form-control"  type="text" value="<?=$detail_pesan['id_pesan']?>" name="id_pesan" readonly>
  </div>
  <div>
    <label class="form-label" >Nama Pemesan</label>
    <input class="form-control"  type="text" value="<?=$detail_pesan['nama_pemesan']?>" readonly>
  </div>
  <div>
    <label class="form-label" >Tanggal Pesan</label>
    <input class="form-control"  type="text" value="<?=$detail_pesan['tanggal_pesan']?>" readonly>
  </div>
  <div>
    <label class="form-label" >Nomor Meja</label>
    <input class="form-control"  type="text" value="<?=$detail_pesan['nm_meja']?>" readonly>
  </div>
  <div>
    <label class="form-label" >Total Bayar</label>
    <input class="form-control"  type="number" value="<?=$total_bayar?>" id="total_bayar" readonly>
  </div>
  <div>
    <label class="form-label" >Dibayar</label>
    <input class="form-control"  type="number" name="dibayar" id="dibayar" />
  </div>
  <div>
    <label class="form-label" >Kembalian</label>
    <input class="form-control"  type="number" name="kembalian" id="kembalian" />
  </div>
  <div>
    <label class="form-label" >Status Pesanan</label>
    <select class="form-control custom-select"  name="status_pesanan">
      <option value="Belum Dihidangkan"<?=$detail_pesan['status_pesanan'] == 'Belum Dihidangkan' ? ' selected="selected"' : '';?>>Belum Dihidangkan</option>
      <option value="Sedang Dipersiapkan"<?=$detail_pesan['status_pesanan'] == 'Sedang Dipersiapkan' ? ' selected="selected"' : '';?>>Sedang Dipersiapkan</option>
      <option value="Siap Dihidangkan"<?=$detail_pesan['status_pesanan'] == 'Siap Dihidangkan' ? ' selected="selected"' : '';?>>Siap Dihidangkan</option>
      <option value="Sudah Dihidangkan"<?=$detail_pesan['status_pesanan'] == 'Sudah Dihidangkan' ? ' selected="selected"' : '';?>>Sudah Dihidangkan</option>
    </select>
  </div>
  <div>
    <label class="form-label" >Status Pembayaran</label>
    <select class="form-control custom-select"  name="status_pembayaran">
      <option value="Belum Dibayar"<?=$detail_pesan['status_pembayaran'] == 'Belum Dibayar' ? ' selected="selected"' : '';?>>Belum Dibayar</option>
      <option value="Sudah Dibayar"<?=$detail_pesan['status_pembayaran'] == 'Sudah Dibayar' ? ' selected="selected"' : '';?>>Sudah Dibayar</option>
    </select>
  </div>
  <button class="btn btn-primary  type="submit">Proses Pesanan</button>
  <a href="<?=$alamat_web?>/pembayaran/cetak/index.php?id_pesan=<?=$detail_pesan['id_pesan']?>">Cetak Invoice</button>
</form>
<script>
function hitungKembalian(){
  var total_bayar = document.getElementById("total_bayar");
  var dibayar = document.getElementById("dibayar");
  var kembalian = document.getElementById("kembalian");
  var total_tmp = dibayar.value - total_bayar.value;
  if(total_tmp >= 0){
    kembalian.value = total_tmp;
  }else{
    kembalian.value = 0;
  }
  
}
document.getElementById("dibayar").addEventListener("keypress", hitungKembalian, false);
</script>

  <?php include("../template/script.php"); ?>
</body>
</html>
