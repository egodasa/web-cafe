<?php
  session_start();
  require("../pengaturan/helper.php");
  cekIzinAksesHalaman(array('Kasir'), $alamat_web);
  $judul_halaman = "Tambah Menu";
  require("../pengaturan/database.php");
  // Ambil daftar kategori
  $query = $db->prepare("SELECT * FROM tbl_kategori"); 
  $query->execute();
  $kategori = $query->fetchAll();
?>
<html>
<head>
  <?php
    include("../template/head.php");
  ?>
</head>
<body>
<form class="card"  method="POST" action="<?=$alamat_web?>/menu/proses_tambah.php">
  <div>
    <label class="form-label" >Nama Menu</label>
    <input class="form-control"  type="text" name="nama" required />
  </div>
  <div>
    <label class="form-label" >Kategori</label>
    <select class="form-control custom-select"  name="id_kategori" required>
      <option selected disabled>-- Pilih Kategori Menu --</option>
      <?php foreach($kategori as $d): ?>
        <option value="<?=$d['id_kategori']?>"><?=$d['nm_kategori']?></option>
      <?php endforeach; ?>
    </select>
  </div>
  <div>
    <label class="form-label" >Deskripsi</label>
    <textarea name="deskripsi" required></textarea>
  </div>
  <div>
    <label class="form-label" >Harga</label>
    <input class="form-control"  type="number" name="harga" required />
  </div>
  <div>
    <label class="form-label" >Gambar</label>
    <input class="form-control"  type="text" name="gambar" required />
  </div>
  <div>
    <button class="btn btn-primary  type="submit">Simpan</button>
    <button class="btn btn-primary  type="reset">Reset</button>
  </div>
</form>
  <?php include("../template/script.php"); ?>
</body>
</html>
