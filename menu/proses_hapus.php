<?php
require("../pengaturan/database.php");
require("../pengaturan/helper.php");

if(isset($_GET['id_menu'])){
  $query = $db->prepare("DELETE FROM tbl_menu WHERE id_menu = :id_menu");
  $query->bindParam("id_menu", $_GET['id_menu']);
  $query->execute();
}

// Arahkan menu ke halaman menu kembali
header("Location: $alamat_web/menu");
?>

