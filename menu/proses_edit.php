<?php
require("../pengaturan/database.php");
require("../pengaturan/helper.php");
if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['id_menu'])){
    $query = $db->prepare("UPDATE tbl_menu SET nama = :nama, id_kategori = :id_kategori, deskripsi = :deskripsi, harga = :harga, gambar = :gambar WHERE id_menu = :id_menu");
    $query->bindParam("id_menu", $_POST['id_menu'], PDO::PARAM_INT);
    $query->bindParam("id_kategori", $_POST['id_kategori'], PDO::PARAM_INT);
    $query->bindParam("nama", $_POST['nama']);
    $query->bindParam("deskripsi", $_POST['deskripsi']);
    $query->bindParam("harga", $_POST['harga'], PDO::PARAM_INT);
    $query->bindParam("gambar", $_POST['gambar']);
    $query->execute();
}

// Arahkan menu ke halaman menu kembali
header("Location: $alamat_web/menu");
?>

