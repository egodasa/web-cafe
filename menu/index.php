<?php
  session_start();
  require("../pengaturan/helper.php");
  cekIzinAksesHalaman(array('Kasir'), $alamat_web);
  $judul_halaman = "Daftar Menu";
  require("../pengaturan/database.php");
  $query = $db->prepare("SELECT menu.*, kategori.nm_kategori FROM tbl_menu menu JOIN tbl_kategori kategori on menu.id_kategori = kategori.id_kategori"); 
  $query->execute();
  $data = $query->fetchAll();
?>
<html>
<head>
  <?php
    include("../template/head.php");
  ?>
</head>
<body>
<a href="<?=$alamat_web?>/menu/tambah.php">Tambah</a>
<table class="table card-table table-vcenter text-nowrap" >
  <thead>
    <tr>
      <th>No</th>
      <th>Nama</th>
      <th>Kategori</th>
      <th>Harga</th>
      <th>Deskripsi</th>
      <th>Gambar</th>
      <th>Aksi</th>
    </tr>
  </thead>
  <tbody>
<?php
$no = 1;
if(count($data) > 0){
  foreach($data as $d){
?>
    <tr>
      <td><?=$no?></td>
      <td><?=$d['nama']?></td>
      <td><?=$d['nm_kategori']?></td>
      <td><?=rupiah($d['harga'])?></td>
      <td><?=$d['deskripsi']?></td>
      <td><?=$d['gambar']?></td>
      <td>
        <a href="<?=$alamat_web?>/menu/proses_hapus.php?id_menu=<?=$d[id_menu]?>">Hapus</a> 
        | <a href="<?=$alamat_web?>/menu/edit.php?id_menu=<?=$d[id_menu]?>">Edit</a></td>
    </tr>
<?php 
  $no++;
  }
}else{
?>
    <tr>
      <td colspan=3>Tidak ada data yang ditampilkan!</td>
    </tr>
<?php
}
?>
  </tbody>
</table>
  <?php include("../template/script.php"); ?>
</body>
</html>
