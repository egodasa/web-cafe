<?php
  session_start();
  require("../pengaturan/helper.php");
  cekIzinAksesHalaman(array('Kasir'), $alamat_web);
  $judul_halaman = "Edit Menu";
  if(isset($_GET['id_menu'])){
    require_once("../pengaturan/database.php");
    // ambil detail menu
    $query1 = $db->prepare("SELECT * FROM tbl_menu WHERE id_menu = ? LIMIT 1"); 
    $query1->bindParam(1, $_GET['id_menu']);
    $query1->execute();
    $detail = $query1->fetch();
    
    // Ambil daftar kategori
    $query2 = $db->prepare("SELECT * FROM tbl_kategori"); 
    $query2->execute();
    $kategori = $query2->fetchAll();
    
    // cek dulu, datanya ketemu atau tidak. Kalau gk ketemu, ya redirect ke halaman awal
    if(empty($detail)){
      header('Location: $alamat_web/menu');
    }
  }else{
    header('Location: $alamat_web/menu');
  }
?>
<html>
<head>
  <?php
    include("../template/head.php");
  ?>
</head>
<body>
<form class="card"  method="POST" action="<?=$alamat_web?>/menu/proses_edit.php">
  <input class="form-control"  type="hidden" name="id_menu" value="<?=$detail['id_menu']?>" />
  <div>
    <label class="form-label" >Nama Menu</label>
    <input class="form-control"  type="text" name="nama" value="<?=$detail['nama']?>" required />
  </div>
  <div>
    <label class="form-label" >Kategori</label>
    <select class="form-control custom-select"  name="id_kategori" required>
      <option selected disabled>-- Pilih Kategori Menu --</option>
      <?php foreach($kategori as $d): ?>
        <option value="<?=$d['id_kategori']?>"<?=$detail['id_kategori'] == $d['id_kategori'] ? ' selected="selected"' : '';?>><?=$d['nm_kategori']?></option>
      <?php endforeach; ?>
    </select>
  </div>
  <div>
    <label class="form-label" >Deskripsi</label>
    <textarea name="deskripsi" required><?=$detail['deskripsi']?></textarea>
  </div>
  <div>
    <label class="form-label" >Harga</label>
    <input class="form-control"  type="number" name="harga" value="<?=$detail['harga']?>" required />
  </div>
  <div>
    <label class="form-label" >Gambar</label>
    <input class="form-control"  type="text" name="gambar" value="<?=$detail['gambar']?>" required />
  </div>
  <div>
    <button class="btn btn-primary"   type="submit">Simpan perubahan</button>
    <button class="btn btn-primary"   type="reset">Reset</button>
  </div>
</form>
  <?php include("../template/script.php"); ?>
</body>
</html>
