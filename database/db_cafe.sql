-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 30, 2019 at 04:35 PM
-- Server version: 5.7.25-log
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mandanon_bbq`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_detail_pesan`
--

CREATE TABLE `tbl_detail_pesan` (
  `id_detail` int(11) NOT NULL,
  `id_pesan` varchar(50) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_detail_pesan`
--

INSERT INTO `tbl_detail_pesan` (`id_detail`, `id_pesan`, `id_menu`, `jumlah`) VALUES
(1, '261218143618416800', 25, 1),
(2, '030119091426062000', 17, 1),
(3, '030119094431889500', 17, 1),
(4, '030119094431889500', 18, 1),
(5, '030119094431889500', 19, 1),
(6, '030119214824000000', 2, 1),
(7, '040119023009322700', 2, 1),
(8, '040119023009322700', 3, 1),
(9, '040119023009322700', 18, 1),
(10, '040119023009322700', 21, 1),
(14, '040119030755015100', 2, 1),
(15, '040119030755015100', 3, 2),
(17, '040119023512016600', 2, 1),
(18, '040119023512016600', 3, 2),
(19, '040119023512016600', 5, 2),
(20, '040119023512016600', 23, 1),
(21, '040119023512016600', 24, 1),
(24, '040119034824363500', 2, 10),
(25, '040119034824363500', 3, 8),
(26, '040119034824363500', 24, 10),
(27, '040119034824363500', 18, 50),
(28, '040119034824363500', 15, 8),
(29, '040119034824363500', 17, 9),
(31, '040119040601572100', 47, 98),
(32, '040119063807309100', 2, 1),
(33, '040119063807309100', 3, 1),
(35, '050119072322352000', 2, 1),
(36, '050119072322352000', 4, 1),
(37, '050119072322352000', 17, 1),
(38, '050119091220104900', 24, 1),
(39, '050119091220104900', 33, 1),
(41, '060119143117970800', 2, 1),
(42, '060119143117970800', 3, 1),
(44, '070119082304558900', 2, 1),
(45, '070119082304558900', 3, 1),
(47, '070119123831270200', 3, 1),
(48, '070119123831270200', 3, 1),
(49, '070119123831270200', 4, 1),
(50, '070119123831270200', 17, 1),
(51, '070119123831270200', 21, 1),
(54, '070119140710504100', 3, 1),
(55, '070119140710504100', 4, 1),
(56, '070119140710504100', 18, 1),
(57, '070119140710504100', 20, 1),
(61, '080119065233016700', 2, 1),
(62, '080119065233016700', 3, 1),
(63, '080119065233016700', 22, 1),
(64, '080119065233016700', 30, 1),
(68, '160119082528958300', 2, 1),
(69, '160119082528958300', 4, 1),
(70, '160119082528958300', 21, 1),
(71, '160119082528958300', 15, 3),
(72, '160119082528958300', 24, 4),
(75, '160119111559110100', 2, 1),
(76, '160119111835665600', 2, 1),
(77, '160119111835665600', 3, 1),
(78, '160119111835665600', 3, 1),
(79, '160119111835665600', 19, 1),
(83, '160119112737302800', 3, 1),
(84, '160119112737302800', 11, 1),
(86, '160119115429958800', 3, 1),
(87, '160119115429958800', 21, 1),
(88, '220119101016342300', 2, 1),
(89, '230119095031092900', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_detail_pesan_tmp`
--

CREATE TABLE `tbl_detail_pesan_tmp` (
  `id_tmp` int(11) NOT NULL,
  `id_pesan` varchar(100) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_detail_pesan_tmp`
--

INSERT INTO `tbl_detail_pesan_tmp` (`id_tmp`, `id_pesan`, `id_menu`, `jumlah`) VALUES
(7, '030119094543434600', 17, 1),
(14, '030119152609660200', 2, 1),
(15, '030119165159477200', 3, 2),
(17, '030119194223232100', 2, 1),
(18, '030119194223232100', 3, 1),
(19, '030119194223232100', 2, 1),
(20, '030119194223232100', 3, 1),
(61, '080119064146334700', 2, 1),
(71, '160119083100058100', 2, 1),
(72, '160119083100058100', 3, 1),
(73, '160119083100058100', 4, 1),
(74, '160119083100058100', 5, 1),
(75, '170119101635517800', 3, 1),
(76, '170119101635517800', 5, 1),
(77, '170119101635517800', 17, 1),
(78, '170119101635517800', 17, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kategori`
--

CREATE TABLE `tbl_kategori` (
  `id_kategori` int(11) NOT NULL,
  `nm_kategori` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_kategori`
--

INSERT INTO `tbl_kategori` (`id_kategori`, `nm_kategori`) VALUES
(1, 'Minuman'),
(2, 'Makanan');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_meja`
--

CREATE TABLE `tbl_meja` (
  `id_meja` int(11) NOT NULL,
  `nm_meja` varchar(10) NOT NULL,
  `kd_meja` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_meja`
--

INSERT INTO `tbl_meja` (`id_meja`, `nm_meja`, `kd_meja`) VALUES
(2, 'Meja 1', '12345'),
(3, 'Meja 2', '43214'),
(4, 'Meja 3', '02881'),
(5, 'Meja 4', '00001'),
(6, 'Meja 5', '27171'),
(7, 'Meja 6', '38274'),
(8, 'Meja 7', '31890'),
(9, 'Meja 8', '48991'),
(10, 'Meja 9', '49911'),
(11, 'Meja 10', '48992'),
(12, 'Meja 11', '31412'),
(13, 'Meja 12', '129431'),
(14, 'Meja 13', '83012'),
(15, 'Meja 14', '12392'),
(16, 'Meja 15', 'M10A3'),
(17, 'Meja 16', 'KFC71'),
(18, 'Meja 17', 'F14A3'),
(19, 'Meja 18', 'A10F1'),
(20, 'Meja 19', '19MJA'),
(21, 'Meja 20', 'MJA20');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_menu`
--

CREATE TABLE `tbl_menu` (
  `id_menu` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `deskripsi` text NOT NULL,
  `harga` int(11) NOT NULL,
  `gambar` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_menu`
--

INSERT INTO `tbl_menu` (`id_menu`, `nama`, `id_kategori`, `deskripsi`, `harga`, `gambar`) VALUES
(2, 'Ayam Bakar Paha', 2, 'Ayam Bakar Paha', 12000, '261218133248164100.jpg'),
(3, 'Ayam Bakar Paha + Nasi', 2, 'Ayam Bakar Paha + Nasi', 18000, '261218133321640800.jpg'),
(4, 'Ayam Bakar Dada', 2, 'Ayam Bakar Dada', 14000, '261218133452780400.jpg'),
(5, 'Ayam Bakar Dada + Nasi', 2, 'Ayam Bakar Dada + Nasi', 20000, '261218133554922700.jpg'),
(6, 'Ayam Penyet Dada', 2, 'Ayam Penyet Dada', 13000, '261218133719425800.jpg'),
(7, 'Ayam Penyet Dada + Nasi', 2, 'Ayam Penyet Dada + Nasi', 18000, '261218133810417600.jpg'),
(8, 'Ayam Penyet Paha', 2, 'Ayam Penyet Paha', 12000, '261218134045165500.jpg'),
(9, 'Ayam Penyet Paha + Nasi', 2, 'Ayam Penyet Paha + Nasi', 18000, '261218134107521100.jpg'),
(10, 'Ikan Bakar', 2, 'Ikan bakar', 13000, '261218134927690600.jpg'),
(11, 'Ikan Bakar + Nasi', 2, 'Ikan bakar + nasi', 18000, '261218134956037400.jpg'),
(12, 'Pecel Lele', 2, 'Pecel lele', 14000, '261218135014643800.jpg'),
(13, 'Pecel Lele + Nasi', 2, 'Pecel lele + nasi', 20000, '261218135039819600.jpg'),
(14, 'Cah Kangkung', 2, 'Cah kangkung', 6000, '261218135108255000.jpg'),
(15, 'Soto', 2, 'Soto', 15000, '261218135129908800.jpg'),
(16, 'Soto Nasi', 2, 'Soto nasi', 18000, '261218135148928400.jpg'),
(17, 'Teh Tarik Es', 1, 'Teh Tarik Es', 7000, '261218140206423200.jpg'),
(18, 'Teh Tarik Panas', 1, 'Teh Tarik Panas', 7000, '261218140229338000.jpg'),
(19, 'Teh Telur', 1, 'Teh Telur', 10000, '261218140251429900.jpg'),
(20, 'Jus Pokat', 1, 'Jus pokat', 10000, '261218140313996200.jpg'),
(21, 'Jus Mangga', 1, 'Jus mangga', 12000, '261218140335227600.jpg'),
(22, 'Jus Tomat', 1, 'Jus Tomat', 7000, '261218140401818600.jpg'),
(23, 'Jus Timun', 1, 'Jus Timun', 7000, '261218140457541300.jpg'),
(24, 'Jus Jeruk', 1, 'Jus Jeruk', 10000, '261218140523228000.jpg'),
(25, 'Teh Es', 1, 'Teh es', 5000, '261218140548237500.jpg'),
(26, 'Kopi Es', 1, 'Kopi Es', 6000, '261218140618621800.jpg'),
(27, 'Kopi Panas', 1, 'Kopi Panas', 5000, '261218140640904300.jpg'),
(28, 'Kopi Susu', 1, 'Kopi Susu', 6000, '261218140701948800.jpg'),
(29, 'Kopi Susu Es', 1, 'Kopi Susu Es', 7000, '261218140731522100.jpg'),
(30, 'Teh Susu', 1, 'Teh Susu', 6000, '261218142046035900.jpg'),
(31, 'Teh Susu Es', 1, 'Teh Susu Es', 7000, '261218142134178000.jpg'),
(32, 'Susu Panas', 1, 'Susu Panas', 5000, '261218142157815600.jpg'),
(33, 'Susu Es', 1, 'Susu Es', 7000, '261218142221418500.jpg'),
(34, 'Jeruk Panas', 1, 'Jeruk Panas', 10000, '261218142247016300.jpg'),
(35, 'Cappucino Es', 1, 'Cappucino Es', 7000, '261218142308960700.jpg'),
(36, 'Cappucino Panas', 1, 'Cappucino Panas', 6000, '261218142331682000.jpg'),
(37, 'Es Lemon Tea', 1, 'Es Lemon Tea', 7000, '261218142357524400.jpg'),
(38, 'Susu Extra Joss', 1, 'Susu Extra Joss', 7000, '261218142416654300.jpg'),
(39, 'Jus Naga', 1, 'Jus Naga', 10000, '261218142440188200.jpg'),
(40, 'Jus Terong Pirus', 1, 'Jus Terong Pirus', 10000, '261218142500222500.jpg'),
(41, 'Jeruk Nipis Es', 1, 'Jeruk Nipis Es', 7000, '261218142517672900.jpg'),
(42, 'Jus Sirsak', 1, 'Jus Sirsak', 10000, '261218142539952800.jpg'),
(43, 'Jus Melon', 1, 'Jus Melon', 100000, '261218142600847000.jpg'),
(44, 'Jus Nenas', 1, 'Jus Nenas', 10000, '261218142622119300.jpg'),
(45, 'Jus Apel', 1, 'Jus Apel', 12000, '261218142643529100.jpg'),
(46, 'Jus Pepaya', 1, 'Jus Pepaya', 10000, '261218142702793900.jpg'),
(47, 'Jus Bengkuang', 1, 'Jus Bengkuang', 100000, '261218142721594700.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pesan`
--

CREATE TABLE `tbl_pesan` (
  `id_pesan` varchar(50) NOT NULL,
  `tanggal_pesan` datetime NOT NULL,
  `nama_pemesan` varchar(100) NOT NULL,
  `status_pesanan` enum('Belum Dibayar','Sudah Dibayar','Hidangan Sedang Disiapkan','Hidangan Sudah Siap') NOT NULL DEFAULT 'Belum Dibayar',
  `id_meja` int(11) NOT NULL,
  `dibayar` int(11) NOT NULL DEFAULT '0',
  `kembalian` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pesan`
--

INSERT INTO `tbl_pesan` (`id_pesan`, `tanggal_pesan`, `nama_pemesan`, `status_pesanan`, `id_meja`, `dibayar`, `kembalian`) VALUES
('030119091426062000', '2018-12-02 16:22:23', 'Mai', 'Sudah Dibayar', 5, 50000, 43000),
('030119094431889500', '2018-12-23 16:45:43', 'ajo', 'Hidangan Sudah Siap', 2, 120000, 96000),
('030119214824000000', '2018-12-18 21:50:40', 'Budiman', 'Belum Dibayar', 2, 0, 0),
('040119023009322700', '2019-01-04 09:35:12', 'Saya', 'Belum Dibayar', 2, 0, 0),
('040119023512016600', '2019-01-04 10:26:38', 'Gue', 'Hidangan Sudah Siap', 2, 110000, 5000),
('040119030755015100', '2019-01-04 10:08:21', 'Ajo', 'Belum Dibayar', 2, 0, 0),
('040119034824363500', '2018-12-24 11:08:19', 'Ranny', 'Hidangan Sudah Siap', 5, 900000, 3000),
('040119040601572100', '2019-01-04 11:10:04', 'Akuh', 'Hidangan Sudah Siap', 2, 9800000, 0),
('040119063807309100', '2019-01-04 14:45:01', 'Budi', 'Belum Dibayar', 2, 0, 0),
('050119072322352000', '2019-01-05 14:25:44', 'BUDI', 'Hidangan Sedang Disiapkan', 2, 40000, 7000),
('050119091220104900', '2018-12-25 16:13:54', 'Anggun', 'Belum Dibayar', 2, 0, 0),
('060119143117970800', '2019-01-06 21:31:41', 'Mai', 'Sudah Dibayar', 2, 50000, 20000),
('070119082304558900', '2019-01-07 15:23:58', 'rrr', 'Sudah Dibayar', 2, 30000, 0),
('070119123831270200', '2019-01-07 19:44:06', 'm budi', 'Sudah Dibayar', 2, 70000, 1000),
('070119140710504100', '2019-01-07 21:36:12', 'm budi', 'Belum Dibayar', 2, 0, 0),
('080119065233016700', '2019-01-08 13:54:13', 'm budii', 'Hidangan Sudah Siap', 2, 50000, 7000),
('160119082528958300', '2019-01-16 15:31:00', 'aku', 'Hidangan Sudah Siap', 2, 130000, 7000),
('160119111559110100', '2019-01-16 18:18:35', 'Budi', 'Hidangan Sudah Siap', 2, 20000, 8000),
('160119111835665600', '2019-01-16 18:49:11', 'Mbudi', 'Hidangan Sudah Siap', 2, 60000, 2000),
('160119112737302800', '2019-01-16 18:55:09', 'anggun', 'Hidangan Sudah Siap', 5, 40000, 4000),
('160119115429958800', '2019-01-16 18:56:30', 'Indah', 'Hidangan Sudah Siap', 7, 30000, 0),
('220119101016342300', '2018-12-09 17:12:42', 'Suardinata', 'Hidangan Sudah Siap', 2, 20000, 8000),
('230119095031092900', '2019-01-23 16:58:10', 'A', 'Sudah Dibayar', 2, 12000, 0),
('261218143618416800', '2018-12-26 21:42:51', 'Mai', '', 2, 6000, 1000);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(10) NOT NULL,
  `password` text NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jk` enum('Laki-laki','Perempuan') NOT NULL,
  `nohp` varchar(15) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `level` enum('Kasir','Dapur') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id_user`, `username`, `password`, `nama`, `jk`, `nohp`, `alamat`, `level`) VALUES
(1, 'kasir', 'c7911af3adbd12a035b289556d96470a', 'Kasir', 'Perempuan', '65343', 'ALamat rumah', 'Kasir'),
(2, 'dapur', 'de20b1d289dd6005ba8116085122f144', 'Dapur', 'Laki-laki', '098766', 'Alamat Dapur', 'Dapur'),
(3, 'ajo', '773f25d9509bc4cffd75d843db03c4f4', 'ajo kudun', 'Laki-laki', '081373652309', 'padang', 'Kasir');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_detail_pesan`
--
ALTER TABLE `tbl_detail_pesan`
  ADD PRIMARY KEY (`id_detail`),
  ADD KEY `id_pesan` (`id_pesan`),
  ADD KEY `id_menu` (`id_menu`);

--
-- Indexes for table `tbl_detail_pesan_tmp`
--
ALTER TABLE `tbl_detail_pesan_tmp`
  ADD PRIMARY KEY (`id_tmp`),
  ADD KEY `id_pesan` (`id_pesan`),
  ADD KEY `id_menu` (`id_menu`);

--
-- Indexes for table `tbl_kategori`
--
ALTER TABLE `tbl_kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `tbl_meja`
--
ALTER TABLE `tbl_meja`
  ADD PRIMARY KEY (`id_meja`);

--
-- Indexes for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  ADD PRIMARY KEY (`id_menu`),
  ADD KEY `id_kategori` (`id_kategori`);

--
-- Indexes for table `tbl_pesan`
--
ALTER TABLE `tbl_pesan`
  ADD PRIMARY KEY (`id_pesan`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_detail_pesan`
--
ALTER TABLE `tbl_detail_pesan`
  MODIFY `id_detail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;

--
-- AUTO_INCREMENT for table `tbl_detail_pesan_tmp`
--
ALTER TABLE `tbl_detail_pesan_tmp`
  MODIFY `id_tmp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT for table `tbl_kategori`
--
ALTER TABLE `tbl_kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_meja`
--
ALTER TABLE `tbl_meja`
  MODIFY `id_meja` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  MODIFY `id_menu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_detail_pesan`
--
ALTER TABLE `tbl_detail_pesan`
  ADD CONSTRAINT `tbl_detail_pesan_ibfk_1` FOREIGN KEY (`id_pesan`) REFERENCES `tbl_pesan` (`id_pesan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_detail_pesan_ibfk_2` FOREIGN KEY (`id_menu`) REFERENCES `tbl_menu` (`id_menu`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  ADD CONSTRAINT `tbl_menu_ibfk_1` FOREIGN KEY (`id_kategori`) REFERENCES `tbl_kategori` (`id_kategori`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
