<?php
require("../pengaturan/database.php");
require("../pengaturan/helper.php");

if(isset($_GET['id_user'])){
  $query = $db->prepare("DELETE FROM tbl_user WHERE id_user = :id_user");
  $query->bindParam("id_user", $_GET['id_user']);
  $query->execute();
}

// Arahkan user ke halaman user kembali
header("Location: $alamat_web/user");
?>

