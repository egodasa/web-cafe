<?php
  session_start();
  require("../pengaturan/helper.php");
  cekIzinAksesHalaman(array('Kasir'), $alamat_web);
  $judul_halaman = "Tambah User";
?>
<html>
<head>
  <?php
    include("../template/head.php");
  ?>
</head>
<body>
<form class="card"  method="POST" action="<?=$alamat_web?>/user/proses_tambah.php">
  <div>
    <label class="form-label" >Username</label>
    <input class="form-control"  type="text" name="username" required />
  </div>
  <div>
    <label class="form-label" >Password</label>
    <input class="form-control"  type="password" name="password" required />
  </div>
  <div>
    <label class="form-label" >Nama Lengkap</label>
    <input class="form-control"  type="text" name="nama" required />
  </div>
  <div>
    <label class="form-label" >Jenis Kelamin</label>
    <select class="form-control custom-select"  name="jk" required>
      <option selected disabled>-- Pilih Jenis Kelamin --</option>
      <option value="Laki-laki">Laki-laki</option>
      <option value="Perempuan">Perempuan</option>
    </select>
  </div>
  <div>
    <label class="form-label" >Nomor HP</label>
    <input class="form-control"  type="text" name="nohp" required />
  </div>
  <div>
    <label class="form-label" >Alamat</label>
    <textarea name="alamat" required></textarea>
  </div>
  <div>
    <label class="form-label" >Level</label>
    <select class="form-control custom-select"  name="level" required>
      <option selected disabled>-- Pilih Level User --</option>
      <option value="Kasir">Bagian Kasir</option>
      <option value="Dapur">Bagian Dapur</option>
    </select>
  </div>
  <div>
    <button class="btn btn-primary  type="submit">Simpan</button>
    <button class="btn btn-primary  type="reset">Reset</button>
  </div>
</form>
  <?php include("../template/script.php"); ?>
</body>
</html>
