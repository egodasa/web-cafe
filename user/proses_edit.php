<?php
require("../pengaturan/database.php");
require("../pengaturan/helper.php");
if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['id_user'])){
    $query = $db->prepare("UPDATE tbl_user SET password= md5(:password), nama = :nama, jk = :jk, nohp = :nohp, alamat = :alamat, level = :level WHERE id_user = :id_user");
    $query->bindParam("id_user", $_POST['id_user'], PDO::PARAM_INT);
    $query->bindParam("password", $_POST['password']);
    $query->bindParam("nama", $_POST['nama']);
    $query->bindParam("jk", $_POST['jk']);
    $query->bindParam("nohp", $_POST['nohp']);
    $query->bindParam("alamat", $_POST['alamat']);
    $query->bindParam("level", $_POST['level']);
    $query->execute();
}

// Arahkan user ke halaman user kembali
header("Location: $alamat_web/user");
?>

