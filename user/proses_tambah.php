<?php
require("../pengaturan/database.php");
require("../pengaturan/helper.php");
if($_SERVER['REQUEST_METHOD'] == 'POST'){
  $query = $db->prepare("INSERT INTO tbl_user (username,password,nama,jk,nohp,alamat,level) VALUES (:username,md5(:password),:nama,:jk,:nohp,:alamat,:level)");
  $query->bindParam("username", $_POST['username']);
  $query->bindParam("password", $_POST['password']);
  $query->bindParam("nama", $_POST['nama']);
  $query->bindParam("jk", $_POST['jk']);
  $query->bindParam("nohp", $_POST['nohp']);
  $query->bindParam("alamat", $_POST['alamat']);
  $query->bindParam("level", $_POST['level']);
  $query->execute();
}

// Arahkan user ke halaman user kembali
header("Location: $alamat_web/user");
?>

