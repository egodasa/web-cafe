<?php
  session_start();
  require("../pengaturan/helper.php");
  cekIzinAksesHalaman(array('Kasir'), $alamat_web);
  $judul_halaman = "Edit User";
  if(isset($_GET['id_user'])){
    require_once("../pengaturan/helper.php");
    $query = $db->prepare("SELECT * FROM tbl_user WHERE id_user = ? LIMIT 1"); 
    $query->bindParam(1, $_GET['id_user']);
    $query->execute();
    $detail = $query->fetch();
    
    // cek dulu, datanya ketemu atau tidak. Kalau gk ketemu, ya redirect ke halaman awal
    if(empty($detail)){
      header('Location: $alamat_web/user');
    }
  }else{
    header('Location: $alamat_web/user');
  }
?>
<html>
<head>
  <?php
    include("../template/head.php");
  ?>
</head>
<body>
<form class="card"  method="POST" action="<?=$alamat_web?>/user/proses_edit.php">
  <input class="form-control"  type="hidden" name="id_user" value="<?=$detail['id_user']?>" />
  <div>
    <label class="form-label" >Username</label>
    <input class="form-control"  type="text" name="username" value="<?=$detail['username']?>" readonly />
  </div>
  <div>
    <label class="form-label" >Password</label>
    <input class="form-control"  type="password" name="password" required />
  </div>
  <div>
    <label class="form-label" >Nama Lengkap</label>
    <input class="form-control"  type="text" name="nama" value="<?=$detail['nama']?>" required />
  </div>
  <div>
    <label class="form-label" >Jenis Kelamin</label>
    <select class="form-control custom-select"  name="jk" required>
      <option selected disabled>-- Pilih Jenis Kelamin --</option>
      <option value="Laki-laki"<?=$detail['jk'] == 'Laki-laki' ? ' selected="selected"' : '';?>>Laki-laki</option>
      <option value="Perempuan"<?=$detail['jk'] == 'Perempuan' ? ' selected="selected"' : '';?>>Perempuan</option>
    </select>
  </div>
  <div>
    <label class="form-label" >Nomor HP</label>
    <input class="form-control"  type="text" name="nohp" value="<?=$detail['nohp']?>" required />
  </div>
  <div>
    <label class="form-label" >Alamat</label>
    <textarea name="alamat" required><?=$detail['alamat']?></textarea>
  </div>
  <div>
    <label class="form-label" >Level</label>
    <select class="form-control custom-select"  name="level" required>
      <option selected disabled>-- Pilih Level User --</option>
      <option value="Kasir"<?=$detail['level'] == 'Kasir' ? ' selected="selected"' : '';?>>Bagian Kasir</option>
      <option value="Dapur"<?=$detail['level'] == 'Dapur' ? ' selected="selected"' : '';?>>Bagian Dapur</option>
    </select>
  </div>
  <div>
    <button class="btn btn-primary  type="submit">Simpan perubahan</button>
    <button class="btn btn-primary  type="reset">Reset</button>
  </div>
</form>
  <?php include("../template/script.php"); ?>
</body>
</html>
