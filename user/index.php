<?php
  session_start();
  require("../pengaturan/helper.php");
  cekIzinAksesHalaman(array('Kasir'), $alamat_web);
  $judul_halaman = "Daftar User";
  require("../pengaturan/database.php");
  $query = $db->prepare("SELECT * FROM tbl_user"); 
  $query->execute();
  $data = $query->fetchAll();
?>
<html>
<head>
  <?php
    include("../template/head.php");
  ?>
</head>
<body>
<a href="<?=$alamat_web?>/user/tambah.php">Tambah</a>
<table class="table card-table table-vcenter text-nowrap" >
  <thead>
    <tr>
      <th>No</th>
      <th>Username</th>
      <th>Nama</th>
      <th>Jenis Kelamin</th>
      <th>NOHP</th>
      <th>Alamat</th>
      <th>Level</th>
      <th>Aksi</th>
    </tr>
  </thead>
  <tbody>
<?php
$no = 1;
if(count($data) > 0){
  foreach($data as $d){
?>
    <tr>
      <td><?=$no?></td>
      <td><?=$d['username']?></td>
      <td><?=$d['nama']?></td>
      <td><?=$d['jk']?></td>
      <td><?=$d['nohp']?></td>
      <td><?=$d['alamat']?></td>
      <td><?=$d['level']?></td>
      <td>
        <a href="<?=$alamat_web?>/user/proses_hapus.php?id_user=<?=$d[id_user]?>">Hapus</a> 
        | <a href="<?=$alamat_web?>/user/edit.php?id_user=<?=$d[id_user]?>">Edit</a></td>
    </tr>
<?php 
  $no++;
  }
}else{
?>
    <tr>
      <td colspan=3>Tidak ada data yang ditampilkan!</td>
    </tr>
<?php
}
?>
  </tbody>
</table>
  <?php include("../template/script.php"); ?>
</body>
</html>
