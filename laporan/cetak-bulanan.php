<?php
  require("../pengaturan/database.php");
  require("../pengaturan/helper.php");
  require("../pengaturan/helper.php");
  cekIzinAksesHalaman(array('Kasir'), $alamat_web);
  
  $waktu = date("Y-m-d");
  if(isset($_GET['waktu'])){
    $waktu = $_GET['waktu'];
  }
  // Ambil laporan
  $query = $db->prepare("select day(pesan.tanggal_pesan) as hari, sum((select sum(b.harga) from tbl_detail_pesan a join tbl_menu b on a.id_menu = b.id_menu where a.id_pesan = pesan.id_pesan)) as total_bayar from tbl_pesan pesan where month(pesan.tanggal_pesan) = month(:waktu) and year(pesan.tanggal_pesan) = year(:waktu) group by day(pesan.tanggal_pesan)"); 
  $query->bindParam("waktu", $waktu);
  $query->execute();
  $laporan = $query->fetchAll();

?>
<style>
body{
	font-family: Times New Roman;
}
table {
    border-collapse: collapse;
	margin: 0 auto;
}

table, td, th {
    border: 1px solid black;
}
th {
	background-color: #f0f0f0;
	padding: 13px 5px;
}
td{
	padding: 7px;
}
h1{
	text-align: center;
}
p{
	text-align: center;
}
.ttl{
	width:75%;
}
.ttd{
	width: 25%;
	text-align:center;
	float: right;
}
</style>
<p>
<b><?=$nama_perusahaan?></b>
</p>
<p><?=$alamat_perusahaan?></p>
<hr/>
<p>
Laporan Pemasukan Bulanan <br/> Bulan : <?=namaBulan(date("m", strtotime($waktu)))?>
</p>
<table>
  <thead>
    <tr>
      <th>No</th>
      <th>Tanggal</th>
      <th>Pemasukan</th>
    </tr>
  </thead>
  <tbody>
<?php
$no = 1;
if(count($laporan) > 0){
  $total_bayar = 0;
  foreach($laporan as $d){
  $total_bayar += $d['total_bayar'];
?>
    <tr>
      <td><?=$no?></td>
      <td><?=$d['hari']?></td>
      <td><?=rupiah($d['total_bayar'])?></td>
    </tr>
<?php 
  $no++;
  }
}else{
?>
    <tr>
      <td colspan=3>Tidak ada data yang ditampilkan!</td>
    </tr>
<?php
}
?>
  </tbody>
  <tfoot>
    <tr>
      <td colspan=2 style="text-align: right;"><b>Total Pemasukan</b></td>
      <td><?=rupiah($total_bayar)?></td>
    </tr>
    </tr>
  </tfoot>
</table>
<br/>
<div class="ttd">
<p><?=$kota_perusahaan?>, <?=date("d/m/Y")?></p><br/>
<br/>
<p>Pimpinan
</p>
</div>

