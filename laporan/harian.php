<?php
  session_start();
  $judul_halaman = "Laporan Harian";
  require("../pengaturan/database.php");
  require("../pengaturan/helper.php");
  cekIzinAksesHalaman(array('Kasir'), $alamat_web);
  $waktu = date("Y-m-d");
  $query_string_waktu = null;
  if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $waktu = $_POST['tanggal'];
    $query_string_waktu = "?waktu=".$waktu;
  }
  // Ambil laporan
  $query = $db->prepare("select pesan.*,time(pesan.tanggal_pesan) as jam,(select sum(b.harga) from tbl_detail_pesan a join tbl_menu b on a.id_menu = b.id_menu where a.id_pesan = pesan.id_pesan) as total_bayar from tbl_pesan pesan where date(pesan.tanggal_pesan) = :tanggal_pesan "); 
  $query->bindParam("tanggal_pesan", $waktu);
  $query->execute();
  $laporan = $query->fetchAll();

?>
<html>
<head>
  <?php
    include("../template/head.php");
  ?>
  <link rel="stylesheet" type="text/css" href="<?=$alamat_web?>/assets/css/pikaday.css">
</head>
<body>
  <p>
    <form class="card"  action="" method="POST">
      <div>
        <label class="form-label" >
          Pilih Tanggal
        </label>
        <input class="form-control"  type="text" id="tanggal" name="tanggal" value="<?=$waktu?>" readonly />
      </div>
      <div>
        <button class="btn btn-primary btn-block"   type="submit">Tampilkan</button>
        <a href="<?=$alamat_web?>/laporan/cetak-harian.php<?=$query_string_waktu?>" target="_blank">Cetak</a>
      </div>
    </form>
  </p>
  Laporan Pemasukan Harian <br/> Tanggal : <?=tanggal_indo($waktu)?>
  <table class="table card-table table-vcenter text-nowrap" >
    <thead>
      <tr>
        <th>No</th>
        <th>Nama Pemesan</th>
        <th>Waktu</th>
        <th>Total Bayar</th>
      </tr>
    </thead>
    <tbody>
  <?php
  $no = 1;
  if(count($laporan) > 0){
    $total_bayar = 0;
    foreach($laporan as $d){
    $total_bayar += $d['total_bayar'];
  ?>
      <tr>
        <td><?=$no?></td>
        <td><?=$d['nama_pemesan']?></td>
        <td><?=$d['jam']?></td>
        <td><?=rupiah($d['total_bayar'])?></td>
      </tr>
  <?php 
    $no++;
    }
  }else{
  ?>
      <tr>
        <td colspan=4>Tidak ada data yang ditampilkan!</td>
      </tr>
  <?php
  }
  ?>
    </tbody>
    <tfoot>
      <tr>
        <td colspan=3 style="text-align: right;"><b>Total Pemasukan</b></td>
        <td><?=rupiah($total_bayar)?></td>
      </tr>
      </tr>
    </tfoot>
  </table>
  
  <?php include("../template/script.php"); ?>
  
  <script src="<?=$alamat_web?>/assets/js/moment.js"></script>
  <script src="<?=$alamat_web?>/assets/js/pikaday.js"></script>
  <script>
      var tanggal = new Pikaday({
        field: document.getElementById('tanggal'),
        format: 'YYYY-MM-DD',
        });
  </script>
</body>
</html>
