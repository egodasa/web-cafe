<?php
  session_start();
  require("../pengaturan/helper.php");
  //~ cekIzinAksesHalaman(array('Dapur'), $alamat_web);
  $judul_halaman = "Detail Pesanan";
  if(isset($_GET['id_pesan'])){
    require_once("../pengaturan/database.php");
    // Ambil detail pesan
    $query = $db->prepare("SELECT a.*,b.nm_meja FROM tbl_pesan a JOIN tbl_meja b ON a.id_meja = b.id_meja WHERE id_pesan = :id_pesan LIMIT 1");
    $query->bindParam("id_pesan", $_GET['id_pesan']); 
    $query->execute();
    $detail_pesan = $query->fetch();
    
    
    // Ambil daftar pesan
    $query = $db->prepare("SELECT * FROM daftar_pesanan WHERE id_pesan = :id_pesan");
    $query->bindParam("id_pesan", $_GET['id_pesan']); 
    $query->execute();
    $daftar_pesan = $query->fetchAll();
    
  }else{
    header("Location: $alamat_web/pembayaran");
  }
?>
<html>
<head>
  <?php
    include("../template/head.php");
  ?>
</head>
<body>
<h2>Daftar Pesanan</h2>
<table class="table card-table table-vcenter text-nowrap" >
  <thead>
    <tr>
      <th>No</th>
      <th>Nama</th>
      <th>Jumlah</th>
    </tr>
  </thead>
  <tbody>
<?php
$no = 1;
if(count($daftar_pesan) > 0){
  foreach($daftar_pesan as $d){
?>
    <tr>
      <td><?=$no?></td>
      <td><?=$d['nama']?></td>
      <td><?=$d['jumlah']?></td>
    </tr>
<?php 
  $no++;
  }
}else{
?>
    <tr>
      <td colspan=3>Tidak ada data yang ditampilkan!</td>
    </tr>
<?php
}
?>
  </tbody>
</table>
<h2>Detail Pesanan</h2>
<form class="card"  action="<?=$alamat_web?>/dapur/proses-pesanan.php" method="POST">
  <div>
    <label class="form-label">No Pesan</label>
    <input class="form-control" type="text" value="<?=$detail_pesan['id_pesan']?>" name="id_pesan" readonly>
  </div>
  <div>
    <label class="form-label">Nama Pemesan</label>
    <input class="form-control" type="text" value="<?=$detail_pesan['nama_pemesan']?>" readonly>
  </div>
  <div>
    <label class="form-label">Tanggal Pesan</label>
    <input class="form-control" type="text" value="<?=$detail_pesan['tanggal_pesan']?>" readonly>
  </div>
  <div>
    <label class="form-label">Nomor Meja</label>
    <input class="form-control" type="text" value="<?=$detail_pesan['nm_meja']?>" readonly>
  </div>
  <div>
    <label class="form-label">Status Pesanan</label>
    <select class="form-control custom-select"  name="status_pesanan">
      <option value="Belum Dihidangkan"<?=$detail_pesan['status_pesanan'] == 'Belum Dihidangkan' ? ' selected="selected"' : '';?>>Belum Dihidangkan</option>
      <option value="Sedang Dipersiapkan"<?=$detail_pesan['status_pesanan'] == 'Sedang Dipersiapkan' ? ' selected="selected"' : '';?>>Sedang Dipersiapkan</option>
      <option value="Siap Dihidangkan"<?=$detail_pesan['status_pesanan'] == 'Siap Dihidangkan' ? ' selected="selected"' : '';?>>Siap Dihidangkan</option>
      <option value="Sudah Dihidangkan"<?=$detail_pesan['status_pesanan'] == 'Sudah Dihidangkan' ? ' selected="selected"' : '';?>>Sudah Dihidangkan</option>
    </select>
  </div>
  <button class="btn btn-primary btn-block"  type="submit">Proses Pesanan</button>
</form>

  <?php include("../template/script.php"); ?>
</body>
</html>
