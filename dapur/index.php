<?php
  session_start();
  require("../pengaturan/helper.php");
  //~ cekIzinAksesHalaman(array('Dapur'), $alamat_web);
  $judul_halaman = "Daftar Pesanan";
  require("../pengaturan/database.php");

  // Ambil daftar menu
  $sql = "SELECT a.*,b.nm_meja FROM tbl_pesan a JOIN tbl_meja b ON a.id_meja = b.id_meja WHERE 1";
  // Nilai default untuk filter daftar menu
  $filter_nama = "";
  $filter_meja = "";
  $filter_status = "Belum Dihidangkan";
  
  // Filter status pembayaran
  if(isset($_GET['status']) && !empty($_GET['status'])){
    $filter_status = $_GET['status'];
  }
  $sql .= " AND a.status_pesanan = :status";  
  
  // Filter nama 
  if(isset($_GET['nama']) && !empty($_GET['nama'])){
    $filter_nama = $_GET['nama'];
    $sql .= " AND a.nama_pemesan LIKE CONCAT('%', :nama ,'%')";
  }
  
  // Filter meja meja
  if(isset($_GET['meja']) && !empty($_GET['meja'])){
    $filter_meja = $_GET['meja'];
    $sql .= " AND b.nm_meja LIKE CONCAT('%', :meja ,'%')";
  }
  
  $sql .= " ORDER BY a.tanggal_pesan ASC";
  
  // Eksekusi query daftar menu beserta filternya
  $query = $db->prepare($sql);
  // filter status tetap dilakukan
  $query->bindParam("status", $filter_status);
  
  if(isset($_GET['nama']) && !empty($_GET['nama'])){
    $query->bindParam("nama", $filter_nama);
  }
  if(isset($_GET['meja']) && !empty($_GET['meja'])){
    $query->bindParam("meja", $filter_meja);
  }
  $query->execute();
  $daftar_pesan = $query->fetchAll();

?>
<html>
<head>
  <?php
    include("../template/head.php");
  ?>
</head>
<body>
<form class="card"  method="GET">
  <div>
    <label class="form-label">Nama Pemesan</label>
    <input class="form-control" type="text" name="nama" value="<?=$filter_nama?>" />
  </div>
  <div>
    <label class="form-label">Nomor Meja</label>
    <input class="form-control" type="text" name="meja" value="<?=$filter_meja?>" />
  </div>
  <div>
    <label class="form-label">Status Pesanan</label>
    <select class="form-control custom-select"  name="status">
      <option value="Belum Dihidangkan"<?=$filter_status == 'Belum Dihidangkan' ? ' selected="selected"' : '';?>>Belum Dihidangkan</option>
      <option value="Sedang Dipersiapkan"<?=$filter_status == 'Sedang Dipersiapkan' ? ' selected="selected"' : '';?>>Sedang Dipersiapkan</option>
      <option value="Siap Dihidangkan"<?=$filter_status == 'Siap Dihidangkan' ? ' selected="selected"' : '';?>>Siap Dihidangkan</option>
      <option value="Sudah Dihidangkan"<?=$filter_status == 'Sudah Dihidangkan' ? ' selected="selected"' : '';?>>Sudah Dihidangkan</option>
    </select>
  </div>
  <button class="btn btn-primary btn-block"  type="submit">Tampilkan</button>
  <a href="<?=$alamat_web?>/dapur">Reset</a>
</form>
<h2>Daftar Pesanan</h2>
<table class="table card-table table-vcenter text-nowrap" >
  <thead>
    <tr>
      <th></th>
    </tr>
    <tr>
      <th>No</th>
      <th>Nama Pemesan</th>
      <th>Waktu Pesan</th>
      <th>Nomor Meja</th>
      <th>Status Pesanan</th>
      <th>Aksi</th>
    </tr>
  </thead>
  <tbody>
<?php
$no = 1;
if(count($daftar_pesan > 0)){
  foreach($daftar_pesan as $d){
?>
    <tr>
      <td><?=$no?></td>
      <td><?=$d['nama_pemesan']?></td>
      <td><?=$d['tanggal_pesan']?></td>
      <td><?=$d['nm_meja']?></td>
      <td><?=$d['status_pesanan']?></td>
      <td>
        <a href="<?=$alamat_web?>/dapur/detail-pesan.php?id_pesan=<?=$d['id_pesan']?>">Detail Pesanan</a>
    </tr>
<?php 
  $no++;
  }
}else{
?>
    <tr>
      <td colspan=7>Tidak ada data yang ditampilkan!</td>
    </tr>
<?php
}
?>
  </tbody>
</table>

  <?php include("../template/script.php"); ?>
</body>
</html>
