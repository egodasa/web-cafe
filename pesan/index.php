<?php
  session_start();
  require("../pengaturan/helper.php");
  $judul_halaman = "Pemesanan Menu";
  require("../pengaturan/database.php");
  if(!isset($_SESSION['id_pesan'])){
    if(empty(($_SESSION['id_pesan']))){
      $_SESSION['id_pesan'] = generateNumber();
    }
  }
  
  // Ambil daftar menu
  $sql = "SELECT menu.*, kategori.nm_kategori FROM tbl_menu menu JOIN tbl_kategori kategori on menu.id_kategori = kategori.id_kategori WHERE 1";
  // Nilai default untuk filter daftar menu
  $filter_kategori = 0;
  $filter_nama = "";
  $filter_min = "";
  
  // Filter kategori
  if(isset($_GET['kategori']) && !empty($_GET['kategori'])){
    $filter_kategori = $_GET['kategori'];
    $sql .= " AND menu.id_kategori = :kategori";
  }
  // Filter nama
  if(isset($_GET['nama']) && !empty($_GET['nama'])){
    $filter_nama = $_GET['nama'];
    $sql .= " AND menu.nama LIKE CONCAT('%', :nama ,'%')";
  }
  // Filter harga minimal
  if(isset($_GET['min']) && !empty($_GET['min'])){
    $filter_min = $_GET['min'];
    $sql .= " AND menu.harga >= :min";
  }
  // Filter harga maksimal
  if(isset($_GET['max']) && !empty($_GET['max'])){
    $filter_max = $_GET['max'];
    $sql .= " AND menu.harga <= :max";
  }
  // Filter urutan harga
  if(isset($_GET['harga']) && !empty($_GET['harga'])){
    $filter_harga = $_GET['harga'];
    if($_GET['harga'] == "termurah"){
      $sql .= " ORDER BY menu.harga ASC";
    }else{
      $sql .= " ORDER BY menu.harga DESC";
    }
  }
  
  // Eksekusi query daftar menu beserta filternya
  $query = $db->prepare($sql);
  if(isset($_GET['kategori']) && !empty($_GET['kategori'])){
    $query->bindParam("kategori", $filter_kategori, PDO::PARAM_INT);
  }
  if(isset($_GET['nama']) && !empty($_GET['nama'])){
    $query->bindParam("nama", $filter_nama);
  }
  if(isset($_GET['min']) && !empty($_GET['min'])){
    $query->bindParam("min", $filter_min);
  }
  if(isset($_GET['max']) && !empty($_GET['max'])){
    $query->bindParam("max", $filter_max);
  }
  $query->execute();
  $daftar_menu = $query->fetchAll();
  
  // Ambil daftar kategori
  $query = $db->prepare("SELECT * FROM tbl_kategori"); 
  $query->execute();
  $kategori = $query->fetchAll();
  
  // Ambil daftar pesanan
  $query = $db->prepare("SELECT * FROM daftar_pesanan_tmp");
  $query->execute();
  $data_pesanan = $query->fetchAll();
  
  // Ambil daftar meja
  $query = $db->prepare("SELECT * FROM tbl_meja");
  $query->execute();
  $data_meja = $query->fetchAll();
?>
<html>
<head>
  <?php
    include("../template/head.php");
  ?>
</head>
<body>
<form class="card"  method="GET">
  <div>
    <label class="form-label" >Nama Menu</label>
    <input class="form-control"  type="text" name="nama" value="<?=$filter_nama?>"/>
  </div>
  <div>
    <label class="form-label" >Kategori</label>
    <select class="form-control custom-select"  name="kategori" required>
      <option value="0"<?=$filter['kategori'] == 0 ? ' selected="selected"' : '';?>>-- Semua Kategori Menu --</option>
      <?php foreach($kategori as $d): ?>
        <option value="<?=$d['id_kategori']?>"<?=$filter_kategori == $d['id_kategori'] ? ' selected="selected"' : '';?>><?=$d['nm_kategori']?></option>
      <?php endforeach; ?>
    </select>
  </div>
  <div>
    <label class="form-label" >Urutan Harga</label>
    <select class="form-control custom-select"  name="harga" required>
      <option selected disabled>-- Pilih Urutan --</option>
      <option value="termurah"<?=$filter_harga == 'termurah' ? ' selected="selected"' : '';?>>Termurah</option>
      <option value="termahal"<?=$filter_harga == 'termahal' ? ' selected="selected"' : '';?>>Termahal</option>
    </select>
  </div>
  <div>
    <label class="form-label" >Harga Minimal</label>
    <input class="form-control"  type="number" name="min" value="<?=$filter_min?>" />
  </div>
  <div>
    <label class="form-label" >Harga Maksimal</label>
    <input class="form-control"  type="number" name="max" value="<?=$filter_max?>" />
  </div>
  <button class="btn btn-primary  type="submit">Tampilkan</button>
  <a href="<?=$alamat_web?>/pesan">Reset</a>
</form>
<h2>Daftar Menu</h2>
<table class="table card-table table-vcenter text-nowrap" >
  <thead>
    <tr>
      <th></th>
    </tr>
    <tr>
      <th>No</th>
      <th>Nama</th>
      <th>Kategori</th>
      <th>Harga</th>
      <th>Deskripsi</th>
      <th>Gambar</th>
      <th>Aksi</th>
    </tr>
  </thead>
  <tbody>
<?php
$no = 1;
if(count($daftar_menu) > 0){
  foreach($daftar_menu as $d){
?>
    <tr>
      <td><?=$no?></td>
      <td><?=$d['nama']?></td>
      <td><?=$d['nm_kategori']?></td>
      <td><?=rupiah($d['harga'])?></td>
      <td><?=$d['deskripsi']?></td>
      <td><?=$d['gambar']?></td>
      <td>
        <form class="card"  action="<?=$alamat_web?>/pesan/tambah-pesan.php" method="POST" name="tambah_pesan">
          <input class="form-control"  type="hidden" value="<?=$d['id_menu']?>" name="id_menu" />
          <input class="form-control"  type="hidden" value="<?=$_SESSION['id_pesan']?>" name="id_pesan" />
          <input class="form-control"  type="number" min=1 name="jumlah" placeholder="Jumlah" required />
          <button class="btn btn-primary  type="submit">Tambah Pesanan</button>
        </form>
    </tr>
<?php 
  $no++;
  }
}else{
?>
    <tr>
      <td colspan=7>Tidak ada data yang ditampilkan!</td>
    </tr>
<?php
}
?>
  </tbody>
</table>

<h2>Daftar Pesanan</h2>
<table class="table card-table table-vcenter text-nowrap" >
  <thead>
    <tr>
      <th>No</th>
      <th>Nama</th>
      <th>Harga</th>
      <th>Sub Bayar</th>
      <th>Aksi</th>
    </tr>
  </thead>
  <tbody>
<?php
$no = 1;
if(count($data_pesanan) > 0){
  $total_bayar = 0;
  foreach($data_pesanan as $d){
  $total_bayar += $d['sub_bayar'];
?>
    <tr>
      <td><?=$no?></td>
      <td><?=$d['nama']?></td>
      <td><?=rupiah($d['harga'])?></td>
      <td><?=rupiah($d['sub_bayar'])?></td>
      <td>
        <a href="<?=$alamat_web?>/pesan/hapus-pesan.php?id_tmp=<?=$d['id_tmp']?>">Hapus</a>
      </td>
    </tr>
<?php 
  $no++;
  }
}else{
?>
    <tr>
      <td colspan=3>Tidak ada data yang ditampilkan!</td>
    </tr>
<?php
}
?>
  </tbody>
  <tfoot>
    <tr>
      <td colspan=4>Total Bayar</td>
      <td><?=rupiah($total_bayar)?></td>
    </tr>
    </tr>
    <tr>
      <td colspan=5>
        <form class="card"  method="POST" action="<?=$alamat_web?>/pesan/simpan-pesan.php">
          <input class="form-control"  type="hidden" name="id_pesan" value="<?=$_SESSION['id_pesan']?>" />
          <div>
            <label class="form-label" >Nomor Pesan</label>
            <input class="form-control"  type="text" name="id_pesan" value="<?=$_SESSION['id_pesan']?>" readonly />
          </div>
          <div>
            <label class="form-label" >Nama Pemesan</label>
            <input class="form-control"  type="text" name="nama_pemesan" />
          </div>
          <div>
            <label class="form-label" >Nomor Meja</label>
            <select class="form-control custom-select"  name="id_meja" required>
              <option selected disabled>-- Pilih Meja --</option>
              <?php foreach($data_meja as $d): ?>
                <option value="<?=$d['id_meja']?>"><?=$d['nm_meja']?></option>
              <?php endforeach; ?>
            </select>
          </div>
          <div>
            <button class="btn btn-primary  type="submit">Selesaikan Pesanan</button>
            <button class="btn btn-primary  type="button"><a href="<?=$alamat_web?>/pesan/reset-pesan.php">Batalkan/Ulang Pesan</a></button>
          </div>
        </form>
      </td>
    </tr>
  </tfoot>
</table>

  <?php include("../template/script.php"); ?>
</body>
</html>
