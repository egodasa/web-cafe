<?php
session_start();
require("../pengaturan/database.php");
require("../pengaturan/helper.php");
if($_SERVER['REQUEST_METHOD'] == 'POST'){
  $db->beginTransaction();
  $query = $db->prepare("INSERT INTO tbl_pesan (id_pesan, tanggal_pesan, nama_pemesan, id_meja) VALUES (:id_pesan, :tanggal_pesan, :nama_pemesan, :id_meja)");
  $query->bindParam("id_pesan", $_POST['id_pesan']);
  $query->bindParam("tanggal_pesan", date("Y-m-d H:i:s"));
  $query->bindParam("nama_pemesan", $_POST['nama_pemesan']);
  $query->bindParam("id_meja", $_POST['id_meja']);
  $query->execute();
  
  $query = $db->prepare("INSERT INTO tbl_detail_pesan (id_pesan, id_menu, jumlah) SELECT id_pesan, id_menu, jumlah FROM tbl_detail_pesan_tmp WHERE id_pesan = :id_pesan");
  $query->bindParam("id_pesan", $_POST['id_pesan']);
  $query->execute();
  
  // Hapus semua data pesan dari tabel pesan_tmp
  $query = $db->prepare("DELETE FROM tbl_detail_pesan_tmp WHERE id_pesan = :id_pesan");
  $query->bindParam("id_pesan", $_POST['id_pesan']); 
  $query->execute();
  
  $db->commit();
  // Buat id_pesan baru
  $_SESSION['id_pesan'] = generateNumber();
}

// Arahkan menu ke halaman menu kembali
header("Location: $alamat_web/pesan");
?>

